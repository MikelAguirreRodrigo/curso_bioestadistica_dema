1. Crea una hoja de cálculo llamada “Reactivos a comprar” e introduce en ella los nombres de los reactivos y su valor en € (invetaros los datos). Introduce después una fórmula que sume todos los valores.
Finalmente, cambia algunos precios de los reactivos y observa el nuevo valor del lotes.
- Utiliza el formato-> celda para crear los bordes a las celdas.
- Cambia el encabezado de la hoja por: ”Precios de los reactivos del lote de compra de mayo” con un borde en color azul.
- Cambia el pie de página de la hoja por el nombre del fichero, con un borde a ambos lados de color verde.
- Fija la primera linea y la primera columna del documento.


2. Observa las distintas pestañas que te ofrece el menú formato->celda. Para realizar la “ficha post Impresion” debes dar a las celdas de algunas columnas la categoría y el formato adecuado en la pestaña numeros como por ejemplo: número con separador de miles para la columna de los códigos o un porcentaje sin decimales para la columna descuento.
- Pon los bordes y fondos que tiene la ficha.
- Modifica el formato de página bien desde el área de trabajo o desde la vista preliminar para que tenga una orienzación horizontal y márgenes de 2 cm a la izquierda y la derecha y de 4 cm arriba y abajo.
- Nombra la hoja como “Pedido 1”


3. En las hojas de cálculo anteriores ya has observado que cuando activas una celda, puedes ver su contenido en la "línea de entrada", y que a su izquierda aparecen los símbolos igual (=), suma (Σ) y piloto automático de funciones o asistente de funciones.
Para crear una fórmula utilizando una de las funciones debes realizar los siguientes pasos:
• Activar la celda que contendrá la fórmula
• Pulsar el botón del autopiloto, lo que abrirá su cuadro de diálogo donde te aparecen todas las funciones que puedes utilizar por orden alfabético. Si haces click sobre alguna de ellas te explica a la derecha qué realiza esa función. Para ver las funciones ordenadas por tipo( matemáticas, estadística, finanzas,etc) lo seleccionas en Categoría.
• Una vez encuentres la función que necesitas pulsas en siguiente y a la derecha te aparecen cuadros donde deberás seleccionar las celdas de la hoja de cálculo que contienen los datos sobre los que se va a realizar la función. Para seleccionarlas pulsa el botón "reducir", selecciona con el ratón la celda o rango de ellas y pulsa "aumentar". Una vez que hayas seleccionado todos los datos pulsa aceptar y obtendrás el resultado.
Realiza la "ficha de compra" utilizando las siguientes funciones:
- SUMA de la categoría Matemáticas
- PROMEDIO de la categoría Estadística
- MÁX de la categoría Estadística


4. Realiza la "ficha Referencias" conociendo la diferencia entre Referencias relativas y absolutas. En las fichas anteriores puedes comprobar cómo las formulas que has empleado tienen referencias relativas ya que si se tiene la fórmula = A3*B2 en una celda y la copias en las celdas hacia abajo ( o arrastras con el ratón) se obtendrían las fórmulas: =A4*B3, =A5*B4, =A6*B5, etc y si las copias hacia la derecha se obtienen las fórmulas: =B3*C2, =C3*D2; =D3*E2, etc.
Una referencia absoluta implica que una de las celdas o varias en una fórmula se van a mantener fijas, y se consigue esto introduciendo el símbolo $ delante y detrás de la celda que ha de quedar fija. Por ejemplo: la fórmula =A1*$E$7 si la copias hacia abajo obtendrás las fórmulas: =A2*$E$7 , =A3*$E$7 ,=A4*$E$7, =A5*$E$7, etc y si la copias hacia la derecha obtendrás las fórmulas: =B1*$E$7 , =C1*$E$7 , =D1*$E$7 , etc.
Del mismo modo, Realiza el mismo ejercicio utilizando la hoja de calculo que has creado con el nombre de "Reactivos a comprar" en el ejercico 1, traslada los valores de forma transpuesta mediante enlace a una segunda hoja.


5. Utiliza la función matemática "CONTAR.SI" sobre los datos obtenidos en la segunda hoja que has creado en el archivos "Reactivos a comprar" y solo cuenta los valores que estén por encima del valore de la media artimetica.


6. Utilizando el documento heart_MA.xlsx adjunto en la carpeta.
- Utiliza la función lógica SI para poner el sexo automaticamente a mujer u hombre dependiendo de si el número del numero categorico.
- Utiliza las funciones SUMA, MÍN, MAX Y PROMEDIO para calcular los totales, máximos, mínimos y medias.
- Utiliza la función matemática CONTAR.SI para calcular automaticamente los casos de coresterol superiores a 230.


7. Crear una tabla dinamica que se tenga en cuenta las siguientes caracteristcas
- División por filas por edades
- División por columnas por sexo
- Filtrado con oldpeak > 0,1
- Promedio de coresterol
- Desviación estandar coresterol
- % de diferencia del promedio de trtbps teniendo como base la edad de 40 años



