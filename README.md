**CURSO DE BIOESTADÍSTICA Y PROGRAMACIÓN EN R**


Esta pagina tiene el objetivo de recoger los principios básicos de estadistica y programación en R aplicada a la biologia.


Los apartados descritos son los siguientes:

- 1. Hojas de cálculo [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/tree/master/Hojas%20de%20c%C3%A1lculo?ref_type=heads)

- 2. Bioestadística [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/tree/master/Bioestadistica?ref_type=heads)

- 3. Programas bioinformáticos [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/tree/master/Programas_bioinformaticos?ref_type=heads)

- 4. Bases de datos biosanitarias [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/tree/master/Bases%20de%20datos%20biosanitarias?ref_type=heads)

- 5. Lenguajes de programación [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/tree/master/Lenguajes_programacion?ref_type=heads)

- 6. Lenguaje de programación R [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/tree/master/Lenguaje%20de%20programacion%20R?ref_type=heads)


Anexos:

- Statistic_sort (esquema simplificado de estadística)

- Statistical_advisor (esquema avanzadode estadística)


---

**Enlaces de refuerzo – ayuda**

**Foro del curso**
- Os he dejado un ejemplo de cómo se envían los mensajes en él [LINK](https://gitlab.com/MikelAguirreRodrigo/curso_bioestadistica_dema/-/issues)


---
0. **Git**

- Git [LINK](https://git-scm.com/)

- Gitlab [LINK](https://about.gitlab.com/)

- Learn Git (from Gitlab) [LINK](https://docs.gitlab.com/ee/topics/git/index.html)

- Github [LINK](https://github.com/)


---
1. **LibreOffice**

- Instalador LibreOffice [LINK](https://es.libreoffice.org/descarga/libreoffice/?type=win-x86&version=7.1.3&lang=es)

- Manuales de LibreOffice [LINK](https://documentation.libreoffice.org/es/documentacion-en-espanol/)

- Curso de LibreOffice Calc (repaso) [LINK](https://www.youtube.com/playlist?list=PLLLaU95AMQPrMifyMRgiwhqKA64g7Kiea)


---
2. **Microsoft Excel (365)**

-  Curso básico de Microsoft Excel [LINK](https://support.microsoft.com/es-es/office/aprendizajes-en-vídeo-de-excel-9bc05390-e94c-46af-a5b3-d7c22f6990bb?ui=es-es&rs=es-es&ad=es)

- Ayuda general de Microsoft Excel [LINK](https://support.microsoft.com/es-es/excel)


---
3. **Lenguaje de programación R**

-  **Instalador de lenguaje R** [LINK](https://cran.r-project.org/)

- [1] Windows [LINK](https://cran.r-project.org/bin/windows/base/)
 En caso de ser usuario de Windows recordar que tambien hay que instalar [Rtools](https://cran.r-project.org/bin/windows/Rtools/) para poder ejecutar scripts de R.

- [2] Mac [LINK](https://cran.r-project.org/bin/macosx/)

- [3] Linux [LINK](https://cran.r-project.org/bin/linux/)

- **Actualización de la versión de R para Windows**

1º- install.packages("installr")

2º- library(installr)

3º- updateR()

- **Instalador de Rstudio** [LINK](https://www.rstudio.com/products/rstudio/download/#download)

- **R-universe** (biblioteca de paquetes de R) [LINK](https://r-universe.dev/search/)

- **WebR** (Para utilizar R en un browser sin instalarlo en local)  [LINK](https://github.com/r-wasm/webr/)


---
4. **Pagina de descarga de ejercicios**

- Kaggle [LINK](https://www.kaggle.com/)

- Data.world [LINK](https://data.world/datasets/open-data)

- Open data Euskadi [LINK](https://opendata.euskadi.eus/catalogo-datos/)

- Open data Bilbao [LINK](https://www.bilbao.eus/opendata/es/inicio)

- Data Planet [LINK](https://dataplanet.sagepub.com/)

- Data First [LINK](https://www.datafirst.uct.ac.za/dataportal/index.php/catalog/central)


---
5. **Manuales de programación y estadística**

- *Libros para Data Science* [LINK](https://t.co/aipnnMNIKB)

- *Manuales libres (de todo tipo y niveles)* [LINK](https://bookdown.org/)

- Manuales libres de R [LINK](https://bookdown.org/home/tags/r-programming/)

- Manuales libres de R (avanzado) [LINK](https://bookdown.org/home/tags/advanced-r/)

- Manuales libres de Estadística [LINK](https://bookdown.org/home/tags/statistics/)

- Manuales libres de Data analysis [LINK](https://bookdown.org/home/tags/data-analysis/)

- Manuales libres de análisis medicina [LINK](https://bookdown.org/home/tags/medicine/)

- Manuales libres de data science [LINK](https://bookdown.org/home/tags/data-science/)

- Computer Science courses [LINK](https://github.com/Developer-Y/cs-video-courses)

- Coding for data [LINK](https://matthew-brett.github.io/cfd2019/)

- Fundamentos de ciencia de datos con R [LINK](https://cdr-book.github.io/index.html)


---
6. **Manuales de R (Selección)**
---
    A) Manuales de programación R:

- Manual de R (amigable 1) [LINK](https://fhernanb.github.io/Manual-de-R/)

- Manual de R (amigable 2) [LINK](https://bookdown.org/matiasandina/R-intro/)

- R para principiantes [LINK](https://bookdown.org/jboscomendoza/r-principiantes4/)

- R Introducción a data science [LINK](https://diegokoz.github.io/intro_ds/)

- Manual de Rstudio [LINK](https://bookdown.org/gboccardo/manual-ED-UCH/)

- Learnr (aprender R desde el paquere de learnr) [LINK](https://github.com/rstudio/learnr/)


---
    B) Manuales de estadística

- Estadística básica [LINK](https://bookdown.org/aquintela/EBE/)

- Estadística Aplicada con R [LINK](https://bookdown.org/oscar_teach/estadistica_aplicada_con_r/)

- Estadística multivariada [LINK](https://est-mult.netlify.app/)

- R exploratory data analysis [LINK](https://bookdown.org/rdpeng/exdata/)

- Estadística y Machine Learning con R [LINK](https://github.com/JoaquinAmatRodrigo/Estadistica-con-R)

- Machine Learning for beginners [LINK](https://github.com/microsoft/ML-For-Beginners)

- Introducción al Machine Learning [LINK](https://code.datasciencedojo.com/Danielhuang69/tutorials/tree/master/Introduction%20to%20Machine%20Learning%20with%20R%20and%20Caret)

- Ciencia de datos [LINK](https://github.com/CristinaGil/Ciencia-de-Datos-R)

- Recursos varios para la ciencia de datos [LINK](https://github.com/camartinezbu/recursos_ciencia_datos)

- Preparación y limpieza de datos [LINK](https://bookdown.org/hcwatt99/Data_Wrangling_Recipes_in_R/)

- Data Science Cheatsheet [LINK](https://github.com/ml874/Data-Science-Cheatsheet)

- Rstudio Posit Cheatsheet [LINK](https://github.com/rstudio/cheatsheets/tree/main)

- R for Non-programmers [LINK](https://bookdown.org/daniel_dauber_io/r4np_book/)

- Intro to Probability for Data Science [LINK](https://probability4datascience.com/index.html)

- Introduction to Datascience: Learn Julia Programming, Math & Datascience from Scratch [LINK](https://datascience-book.gitlab.io/book.html)

- Reproducible Medical Research with R [LINK](https://bookdown.org/pdr_higgins/rmrwr/)


---
    C) Paquetes de R interesantes

- dlookr (diagnostico de datos, transforaciones, summary, etc) [LINK](https://choonghyunryu.github.io/dlookr/index.html)

- probably (analisis de probabilidad) [LINK](https://probably.tidymodels.org/)

- Easystats [LINK](https://easystats.github.io/easystats/)



---
    D) (Twitter)

- R Function a day [LINK](https://twitter.com/rfunctionaday)

- Data Science Dojo [LINK](https://twitter.com/DataScienceDojo)

- Ecology in R [LINK](https://twitter.com/EcologyinR)

- Comunidad R Hispano [LINK](https://twitter.com/R_Hisp)

- Rstudio [LINK](https://twitter.com/rstudio)


---
7. **Gráficos e imágenes en R**

- R graph gallery [LINK](https://www.r-graph-gallery.com/index.html) [LINK](https://r-charts.com/es/)

- Modern Data Visualization with R [LINK](https://rkabacoff.github.io/datavis/)

- Plotly en R [LINK](https://plotly.com/r/)

- Gráficos con ggplot2 [LINK](https://bookdown.org/home/tags/ggplot2/) [LINK](https://exts.ggplot2.tidyverse.org/gallery/)

- Gráficos con ggfortify [LINK](https://github.com/sinhrks/ggfortify)

- Gráficos con smplot2 [LINK](https://github.com/smin95/smplot2)

- Gráficos con rgl [LINK](https://github.com/dmurdoch/rgl)

- Gráficos de timeseries [LINK](https://github.com/danvk/dygraphs)

- Hacer aplicaciones interactuables con shiny (con botones y esas cositas) [LINK](https://deanattali.com/blog/building-shiny-apps-tutorial/)

- Trabajar con imágenes: imager [LINK](https://dahtah.github.io/imager/imager.html) [LINK](https://cran.r-project.org/web/packages/imager/vignettes/gettingstarted.html)

- Trabajar con imágenes: imagerExtra [LINK](https://github.com/ShotaOchi/imagerExtra)

- Trabajar con imágenes: OpenImageR [LINK](https://github.com/mlampros/OpenImageR)

- Trabajar con imágenes: Magick [LINK](https://github.com/ropensci/magick)

- Trabajar con imágenes (OCR): tesseract [LINK](https://github.com/ropensci/tesseract)

- Trabajar con imágenes (análisis de imágen): imagefluency [LINK](https://github.com/stm/imagefluency)

- Trabajar con PDFs: pdftools [LINK](https://github.com/ropensci/pdftools)

- Trabajar con gifs: gifsky [LINK](https://github.com/r-rust/gifski)

- Crear mapas con leaflet [LINK](https://github.com/rstudio/leaflet/)


---
8. **Paginas de bioinformática**

- stackoverflow (foro de informática para problemas/soluciones) [LINK](https://stackoverflow.com/)

- Bioinformatics [LINK](https://www.bioinformatics.org/)

- PSPP (similar al SPSS) [LINK](https://www.gnu.org/software/pspp/)

- JASP (software para análisis estadístico amigable) [LINK](https://jasp-stats.org/)

- Jamovi (software para análisis estadístico amigable) [LINK](https://jamovi.org/)

- SOFA (software para análisis estadístico amigable) [LINK](https://www.sofastatistics.com/home.php)

- BlueSky Statistics (software para análisis estadístico amigable) [LINK](https://www.blueskystatistics.com/)

- Number Analytics (software de estadística orientado a gráficos) [LINK](https://www.numberanalytics.com/)

- Gretl (Gnu Regression, Econometrics and Time-series Library) [LINK](https://gretl.sourceforge.net/)

- Weka (Open-source machine learning software) [LINK](https://ml.cms.waikato.ac.nz/weka/index.html)

- Modeller [LINK](https://salilab.org/modeller/)

- Jalview [LINK](https://www.jalview.org/)

- NumeRe (Framework for numerical computations, data analysis and visualisation) [LINK](https://github.com/numere-org/NumeRe)

- OmicsSuite (Software navaja suiza para el estudio de Ómicas) [LINK](https://github.com/OmicsSuite/)

- Ankhor (Data flow, analysis and graphs) [LINK](https://www.ankhor.com/)

- Veusz (Scientific plotting software) [LINK](https://veusz.github.io/)

- G*power (a tool to compute statistical power analyses) [LINK](https://c4r.io/tool-repository/gpower/)

- Lista de software (Wiki) [LINK](https://en.wikipedia.org/wiki/List_of_open-source_bioinformatics_software)

- Otra lista [LINK](https://www.softwareradius.com/best-bioinformatics-software-and-tools/)

- Un poco de ayuda con todo [LINK](https://overapi.com/)

- Lista de software para gráficos de genes y proteínas [LINK](https://twitter.com/Verukita1/status/1545221198610669568)



Correo para dudas/ preguntas / etc:
Mikel Aguirre Rodrigo:  mikel.aguirre@proton.me

